#include <iostream>
#include <vector>

using namespace std;

int main() {
	int n, q, size, index, row, col;

	cin >> n >> q;

	int *a = new int[q];

	vector<vector<int>> vec1;

	for (int i = 0; i < n; i++) {
		cin >> size;
		vector<int> vec2;
		for (int j = 0; j < size; j++) {
			cin >> index;
			vec2.push_back(index);
		}
		vec1.push_back(vec2);
	}

	for (int i = 0; i < q; i++) {
		cin >> row >> col;
		a[i] = vec1[row][col];
	}

	for (int i = 0; i < q; i++) {
		cout << a[i] << endl;
	}

	delete a;
	system("pause");
}