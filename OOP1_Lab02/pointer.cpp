#include <iostream>

using namespace std;

void update(int *a, int *b) {
	*a += *b;
	if (*b<*a - *b) { *b = *a - *b - *b; }
	else { *b = *b - *a + *b; }
}

int main() {
	int a, b;
	int *pa = &a, *pb = &b;

	cin >> a >> b;
	update(pa, pb);
	cout << a << endl << b << endl;

	system("pause");
}
