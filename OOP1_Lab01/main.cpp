#include<iostream>
#include<fstream>

using namespace std;

/*! \fn int sum(int a[], int size)
\brief Dizi uyelerini toplayip sonucu dondurur.
\param a[] Text dosyasindan alinan sayilardan olusan dizi.
\param size Dizinin sonunu belirler.
*/
int sum(int a[], int size)
{
	int sum = 0;

	for (int i = 0;i < size;i++)
	{
		sum = sum + a[i];
	}
	return sum;
}
/*! \fn int product(int a[], int size)
\brief Dizi uyelerini carpip sonucu dondurur.
\param a[] Text dosyasindan alinan sayilardan olusan dizi.
\param size Dizinin sonunu belirler.
*/
int product(int a[], int size)
{
	int product = 1;

	for (int i = 0;i < size;i++)
	{
		product = product * a[i];
	}
	return product;
}
/*! \fn int smallest(int a[], int size)
\brief Dizi uyelerinin en kucugunu bulup dondurur.
\param a[] Text dosyasindan alinan sayilardan olusan dizi.
\param size Dizinin sonunu belirler.
*/
int smallest(int a[], int size)
{
	int smallest = a[0];

	for (int i = 1;i < size;i++)
	{
		if (a[i] < smallest) smallest = a[i];
	}
	return smallest;
}

int main()
{
	int size, myArray[10];

	ifstream readText("input.txt");
	readText >> size;

	for (int i = 0;i < size;i++)
	{
		readText >> myArray[i];
	}
	readText.close();

	cout << "Sum is " << sum(myArray, size) << endl;
	cout << "Product is " << product(myArray, size) << endl;
	cout << "Average is " << (float)sum(myArray, size) / size << endl;
	cout << "Smallest is " << smallest(myArray, size) << endl;

	system("pause");
}